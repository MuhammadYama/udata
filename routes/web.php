<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('locale/{locale}', function ($locale) {
    Session::put('locale', $locale);
    //setcookie('locale', $locale , 0 , '/');

    return redirect()->back();
});


Route::get('/', 'WebController@index')->name('home');
Route::get('/about', 'WebController@about')->name('about');
Route::get('pages/{service}/service', 'WebController@service')->name('service');
Route::get('/works', 'WebController@works')->name('works');
Route::get('pages/{work}/work', 'WebController@work');
Route::get('/contact', 'WebController@contact')->name('contact');
Route::post('contactUs', 'WebController@store');
Route::post('subscribe', 'WebController@subscribe');
// Route::view("/","index")->name('home');
// // Route::view("/about","pages.about")->name('about');
// Route::view("/service","pages.service")->name('service');
// Route::view("/works","pages.works")->name('works');
// Route::view("/work","pages.work")->name('work');
// Route::view("/contact","pages.contact")->name('contact');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::get('our-works/images/{id}', 'WorkController@images');
    Route::post('our-works/addImage', 'WorkController@addImage');
    Route::get('our-works/images/{id}/add', 'WorkController@add_image');
    Route::get('our-works/{id}/images/{image_id}/deleteImage', 'WorkController@deleteImage');
    Route::get('our-works/{id}/lists', 'WorkController@lists');
    Route::get('our-works/lists/{id}/add', 'WorkController@add_list');
    Route::post('our-works/addList', 'WorkController@addList');
    Route::get('our-works/{id}/lists/{list_id}/deleteList', 'WorkController@deleteList');



    Route::get('services/{id}/features', 'WorkController@features');
    Route::get('services/features/{id}/add', 'WorkController@add_feature');
    Route::post('services/addFeature', 'WorkController@addFeature');
    Route::get('services/{id}/features/{list_id}/deleteFeature', 'WorkController@deleteFeature');
    Route::get('services/{id}/partners', 'WorkController@partners');
    Route::get('services/partners/{id}/add', 'WorkController@add_partner');
    Route::post('services/addPartner', 'WorkController@addPartner');
    Route::get('services/{id}/partners/{image_id}/deletePartner', 'WorkController@deletePartner');
});
