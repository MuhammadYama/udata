@extends('voyager::master') 

@section('page_header')

<div class="container-fluid">
    <h1 class="page-title">
      
    </h1>
    <a href="../features/{{$service->id}}/add/ " class="btn btn-info">
        <span class="glyphicon glyphicon-dashboard"></span>&nbsp;
        إضافة ميزة
    </a>

</div>
@include('voyager::multilingual.language-selector')
@stop

@section('content')
<div class="page-content browse container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-heading">
                    <strong>
                        <h4 style="margin:20px;">
                        المميزات لـ :  {{$service->title}}  
                        </h4>
                    </strong>
                    <div class="panel-body">
                        
                        <table class="table table-info table-striped table-nowrap dataTable ammar-table">

                            <tr class="">
                                <th>#</th>
                                <th>النص</th> 
                                <th>English text</th> 
                                <th>حذف</th>
                            </tr>
                            
                            @php $n=0; @endphp
                            @foreach($features as $feature)
                            @php $n++; @endphp
                            <tr>
                                    <td>{{$n}}</td> 
                                    <td>{{$feature->text}}</td> 
                                    <td>{{$feature->eng_text}}</td>  
                                    <td>
                                    <a href="{{ url('admin/services/'.$feature->service_id.'/features/'.$feature->id.'/deleteFeature') }}" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-trash"></span>&nbsp;
                                    حذف 
                                    </a>
                                </td> 
                            </tr> 
                            @endforeach
                    
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
