@extends('voyager::master')

@section('page_header')

<div class="container-fluid">
    <h1 class="page-title">
       {{$service->title}}  
    </h1>
    @include('voyager::multilingual.language-selector')
</div>

@stop

@section('content')
<div class="page-content browse container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-heading">
                   
                    <div class="panel-body">


                        <form data-toggle="validator" method="post" action="{{ url('admin/services/addPartner') }}" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <input type="hidden" name="service_id" value="{{ $service->id }}">

                                <div class="row">  
                                       
                                        <div class="col-xs-6">
                                            <div class="form-group form-group-lg">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                    اسم الشريك
                                                    </span>
                                                    <input  class="form-control" type="text" name="title" placeholder="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-6">
                                            <div class="form-group form-group-lg">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                    الصورة
                                                    </span>
                                                    <input  class="form-control" type="file" name="img" placeholder="">
                                                </div>
                                            </div>
                                        </div>

                                </div>

                                <br>
                            <button class="btn btn-primary" type="submit">إضافة</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@stop
