@extends('voyager::master')

@section('page_header')

<div class="container-fluid">
    <h1 class="page-title">
      
    </h1>
    <a href="../partners/{{$service->id}}/add/ " class="btn btn-info">
        <span class="glyphicon glyphicon-dashboard"></span>&nbsp;
        إضافة شريك
    </a>

    @include('voyager::multilingual.language-selector')
</div>
<div class="container-fluid">

</div>

@stop

@section('content')
<div class="page-content browse container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-heading">
                    <strong>
                        <h4 style="margin:20px;">
                        صور لـ :  {{$service->title}}  
                        </h4>
                    </strong>
                    <div class="panel-body">
                        
               
                    <table class="table table-info table-striped table-nowrap dataTable ammar-table">

                            <tr class="">
                                <th>#</th>
                                <th>الاسم</th> 
                                <th>الصورة</th> 
                                <th>حذف</th>
                            </tr>
                            
                            @php $n=0; @endphp
                            @foreach($partners as $partner)
                            @php $n++; @endphp
                            <tr>
                                    <td>{{$n}}</td> 
                                    <td>{{$partner->title}}</td> 
                                    <td>
                                        <a  href="{{ Voyager::image( $partner->img ) }}" target="_blank" style="padding: 10px;">
                                            <img src="{{ Voyager::image( $partner->img ) }}" alt="Image" width="100" height="100px">
                                        </a>
                                    </td> 
                                    <td>
                                            <a href="{{ url('admin/services/'.$partner->service_id.'/partners/'.$partner->id.'/deletePartner') }}" class="btn btn-danger">
                                            <span class="glyphicon glyphicon-trash"></span>&nbsp;
                                            حذف 
                                            </a>
                                     </td> 
                            </tr>
                            @endforeach

                    </table>
        
                    
                    
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
