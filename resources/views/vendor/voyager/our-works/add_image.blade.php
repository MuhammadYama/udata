@extends('voyager::master')

@section('page_header')

<div class="container-fluid">
    <h1 class="page-title">
       {{$work->title}}  
    </h1>
    @include('voyager::multilingual.language-selector')
</div>

@stop

@section('content')
<div class="page-content browse container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-heading">
                   
                    <div class="panel-body">


                        <form data-toggle="validator" method="post" action="{{ url('admin/our-works/addImage') }}" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <input type="hidden" name="work_id" value="{{ $work->id }}">

                                <div class="row">  

                                    <div class="form-group form-group-lg">
                                        <div class="col-xs-6">

                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                 الصورة
                                                </span>
                                                <input id="7" class="form-control" type="file" name="img" placeholder="">
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <br>
                            <button class="btn btn-primary" type="submit">إضافة</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@stop
