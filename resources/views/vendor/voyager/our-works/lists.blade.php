@extends('voyager::master')

@section('page_header')

<div class="container-fluid">
    <h1 class="page-title">
      
    </h1>
    <a href="../lists/{{$work->id}}/add/ " class="btn btn-info">
        <span class="glyphicon glyphicon-dashboard"></span>&nbsp;
        إضافة عمل
    </a>

    @include('voyager::multilingual.language-selector')
</div>
<div class="container-fluid">

</div>

@stop

@section('content')
<div class="page-content browse container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-heading">
                    <strong>
                        <h4 style="margin:20px;">
                        الأعمال لـ :  {{$work->title}}  
                        </h4>
                    </strong>
                    <div class="panel-body">
                        
                        <table class="table table-info table-striped table-nowrap dataTable ammar-table">

                            <tr class="">
                                <th>#</th>
                                <th>النص</th> 
                                <th>English text</th> 
                                <th>حذف</th>
                            </tr>
                            
                            @php $n=0; @endphp
                            @foreach($lists as $list)
                            @php $n++; @endphp
                            <tr>
                                    <td>{{$n}}</td> 
                                    <td>{{$list->text}}</td> 
                                    <td>{{$list->eng_text}}</td> 
                                    <td>
                                    <a href="{{ url('admin/our-works/'.$list->work_id.'/lists/'.$list->id.'/deleteList') }}" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-trash"></span>&nbsp;
                                    حذف 
                                    </a>
                                </td> 
                            </tr>
                            @endforeach
                    
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
