@extends('layout')

        @section('seo')
        <meta name="description" content="{{$seo->description ??''}}">
        <meta name="keywords" content="{{$seo->keywords ??''}}">
        <title>{{ __('general.title') }} | {{ __('general.contact') }}</title>
        @endsection

@section('content')



<!-- Page Title -->
<section id="page-title">

    <div class="container clearfix text-center">
        <h1>{{ __('general.contactUs') }}</h1>
        <span> </span>
    </div>

</section><!-- #page-title end -->

<!-- Content -->
<section id="content">

    <div class="content-wrap" style="padding-bottom: 0;">

        <div class="container clearfix">

            <div class="heading-block center">
                <h2>{{ __('general.sendUs') }}</h2>
                <span>  </span>
            </div>

            <div class="row">
                <div class="col-md-4 pt-3">
                    <form action ="{{ url('/contactUs') }}" method="POST">
                    {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="fullname">{{ __('general.name') }}</label>
                            <input type="text" class="form-control" id="fullname"  name="name" placeholder="{{ __('general.name') }}">
                            <small id="emailHelp" class="form-text text-muted"> </small>
                        </div>
                        <div class="form-group">
                            <label for="phone">{{ __('general.phone') }}</label>
                            <input type="number" class="form-control" id="phone" name="phone" placeholder="{{ __('general.phone') }}">
                        </div>
                        <div class="form-group">
                            <label for="email">{{ __('general.email') }}</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="{{ __('general.email') }}">
                        </div>
                        <div class="form-group">
                            <label for="subject">{{ __('general.subject') }}</label>
                            <input type="text" class="form-control" id="subject" name="subject" placeholder="{{ __('general.subject') }}">
                        </div>
                        <div class="form-group">
                            <label for="branch">{{ __('general.branch') }}</label>
                            <select  class="form-control" id="branch" name="branch">
                                <option >{{ __('general.select') }}</option>
                                <option value="فرع جدة">{{ __('general.jeddah') }}</option>
                                <option value="فرع مكة">{{ __('general.makkah') }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="msg">{{ __('general.message') }}</label>
                            <textarea  class="form-control" id="" rows="5" name="message" placeholder="{{ __('general.message') }}"></textarea>
                        </div>
                        <button type="submit" class="button button-rounded  mt-3">{{ __('general.send') }}</button>
                    </form>
                </div>
                <div class="col-md-8">

           
                    <div class="tabs tabs-tb tabs-responsive clearfix" id="tab" data-accordion-style="accordion-bg">

                        <ul class="tab-nav clearfix">
                            <li><a href="#tabs-1"><i class="icon-map-marker1"></i> {{ __('general.jeddah') }}</a></li>
                            <li><a href="#tabs-2"><i class="icon-map-marker1"></i> {{ __('general.makkah') }}</a></li>
                        </ul>

                        <div class="tab-container">
                            <div class="tab-content clearfix" id="tabs-1">

                                <div class="row  clearfix">
                                    <div class="col-lg-8">

                                        <div class="testimonial">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d4418.388107078723!2d39.83530316845578!3d21.37388438979636!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x40a85646640cd21d!2si-ESNAAD!5e0!3m2!1sen!2ssa!4v1558522088727!5m2!1sen!2ssa" width="100%" height="110" frameborder="0" style="border:0" allowfullscreen></iframe>
                                        </div>
                                       

                                    </div>

                                    <div class="col-lg-4">
                                        <div class="opening-table" style=" padding: 20px;">
                                            <div class="heading-block bottommargin-sm nobottomborder">
                                                <h4>{{ __('general.workingHours') }}</h4>
                                                <span></span>
                                            </div>
                                            <div class="time-table-wrap clearfix">
                                                @foreach($workJ as $workHour)
                                                <div class="row time-table">
                                                    <h5 class="col-lg-5">{{$workHour->translate()->day}}</h5>
                                                    <span class="col-lg-7">{{$workHour->open}} - {{$workHour->close}}</span>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                 
                            </div>

                            <div class="tab-content clearfix" id="tabs-2">

                                <div class="row clearfix">
                                    <div class="col-lg-8">
 
                                        <div class="testimonial">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d4418.388107078723!2d39.83530316845578!3d21.37388438979636!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x40a85646640cd21d!2si-ESNAAD!5e0!3m2!1sen!2ssa!4v1558522088727!5m2!1sen!2ssa" width="100%" height="110" frameborder="0" style="border:0" allowfullscreen></iframe>
                                        </div>
                                    
                                       
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="opening-table" style=" padding: 20px;">
                                            <div class="time-table-wrap clearfix">
                                            <div class="heading-block bottommargin-sm nobottomborder">
                                                <h4>{{ __('general.workingHours') }}</h4>
                                                <span></span>
                                            </div>
                                            <div class="time-table-wrap clearfix">
                                                @foreach($workM as $workHour)
                                                <div class="row time-table">
                                                    <h5 class="col-lg-5">{{$workHour->translate()->day}}</h5>
                                                    <span class="col-lg-7">{{$workHour->open}} - {{$workHour->close}}</span>
                                                </div>
                                                @endforeach
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- row -->

        </div>

    </div>

</section><!-- #content end -->



<!-- Footer -->
<footer id="footer">


    <!-- Copyrights -->
    @include('partials.footer')

</footer><!-- #footer end -->

@endsection