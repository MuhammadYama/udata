@extends('layout')

        @section('seo')
        <meta name="description" content="{{$seo->description ??''}}">
        <meta name="keywords" content="{{$seo->keywords ??''}}">
        <title>{{ __('general.title') }} | {{$service->translate()->title}}</title>
        @endsection

@section('content')


<!-- Page Title -->
<section id="page-title">

    <div class="container clearfix text-center">
        <h1>{{ __('general.OurService') }}</h1>
        <span> </span>
        <!-- <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Shortcodes</a></li>
            <li class="breadcrumb-item active" aria-current="page">Sections</li>
        </ol> -->
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap" style="padding-bottom: 0;">

        <div class="container clearfix">

            <div class="col_two_fifth topmargin-sm nobottommargin" style="min-height: 350px">
                <img src="{{ Voyager::image( $service->img ) }}" alt="Image 1">
            </div>

            <div class="col_three_fifth nobottommargin col_last">

                <div class="heading-block topmargin-sm">
                    <h2>{{$service->translate()->title}}</h2> 
                </div>

                <p>{{$service->translate()->text}}</p>


            </div>

        </div>

        <!-- partners -->
        <div class="section" style="margin-bottom: 0;">
            <div class="container">

                <div class="fancy-title title-center title-dotted-border">
                    <h3>{{ __('general.features') }}</h3>
                </div>

            <div class="row">
                @foreach($features as $feature)
                <div class="col-md-4 mb-5">
                    <div class="feature-box fbox-border fbox-light fbox-effect">
                        <div class="fbox-icon">
                            <a href="#"><i class="icon-ok"></i></a>
                        </div>
                        <h3>{{$feature->getLocalFeature()}}</h3>
                        <p> </p>
                    </div>
                </div>
                @endforeach
            </div>


            </div>
        </div>

        <!-- Clients -->
        <div class="section nomargin noborder bgcolor dark" style="padding: 50px 0;">
            <div class="container center clearfix">

                <div class="heading-block">
                    <h2>{{ __('general.OurPartners') }}</h2>
                    <span> </span>
                </div>

                <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="true" data-items-xs="2" data-items-sm="3" data-items-lg="4" data-items-xl="5">
                     @foreach($partners as $partner)
                    <div class="oc-item"> 
                        <a href="#"><img src="{{ Voyager::image( $partner->img ) }}" alt="Image 1"></a>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>

    </div>

</section><!-- #content end -->


<!-- Footer -->
<footer id="footer">


    <!-- Copyrights -->
    @include('partials.footer')

</footer><!-- #footer end -->

@endsection