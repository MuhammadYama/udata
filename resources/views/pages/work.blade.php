@extends('layout')

        @section('seo')
        <title>{{ __('general.title') }} | {{$work->translate()->title}}</title>
        @endsection

@section('content')



<!-- Page Title
============================================= -->
<section id="page-title">

    <div class="container clearfix text-center">
        <h1>{{ __('general.OurWorkDetail') }}</h1>
        <span> </span>
        <!-- <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Shortcodes</a></li>
            <li class="breadcrumb-item active" aria-current="page">Sections</li>
        </ol> -->
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap" style="padding-bottom: 0;">

        <div class="container clearfix">

            <div class="col_two_fifth topmargin-sm nobottommargin" style="min-height: 350px">
                <img src="{{ Voyager::image( $work->main_img ) }}" alt="Image 1">
            </div>

            <div class="col_three_fifth nobottommargin col_last">

                <div class="heading-block topmargin-sm">
                    <h2>{{$work->translate()->title}}</h2> 
                </div>

                <p>{{$work->translate()->text}}</p>


            </div>

        </div>

        <div class="section parallax parallax-bg nomargin notopborder light" data-rellax-speed="2" style="background: url('{{asset('esnaad/images/udata/parallax/3-dark.jpg')}}') center center; padding: 100px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -200px;">
            <div class="container-fluid nomargin nopadding row justify-content-center clearfix">

                <div class="col-md-12 heading-block center topmargin-sm">
                    <h2>{{ __('general.achieved') }}</h2>
                    <span> </span>
                </div>

                <div class="col-md-10 nobottommargin ">

                        <ul class="iconlist row">
                            @foreach($lists as $list)
                            <li class="col-md-4"><i class="icon-play"></i>{{$list->getLocalList()}}</li> 
                            @endforeach
                        </ul> 
                </div>

            </div>
        </div>

        <div class="container">
            <!-- Gallery Slider  -->
            <div class="clearfix">
                <div class="col-md-12 heading-block center topmargin-sm">
                    <h2>{{ __('general.images') }}</h2>
                    <span> </span>
                </div>
                <!-- Flex Thumbs Slider
                ============================================= -->
                <div class="fslider flex-thumb-grid grid-8 mt-4 clearfix" data-pagi="false" data-speed="650" data-pause="3500" data-animation="fade" data-arrows="true" data-thumbs="true">
                    <div class="flexslider">
                        <div class="slider-wrap">

                            <!-- slider loop --> 
                            @foreach($images as $image)
                            <div class="slide" data-thumb="{{ Voyager::image( $image->img ) }}">
                                <!-- Post Article -->
                                <article class="entry mb-0">
                                    <img src="{{ Voyager::image( $image->img ) }}" alt="Image">
                                </article>
                            </div> 
                            @endforeach
                          
                        </div>
                    </div>
                </div> <!-- Flex Slider End -->
            </div>

        </div>
        
        <!-- download pdf -->
        <div class="container clearfix my-5">
            <div class="promo bg-dark p-5">
                <h3><span class="text-light">{{ __('general.completion') }}</span></h3>
                <!-- <span>We strive to provide Our Customers with Top Notch Support to make their Theme Experience Wonderful</span> -->
                <?php $file = (json_decode($work->completion))[0]->download_link; ?>
                <a href="{{ Voyager::image( $file ) }}" target="_blank" class="button button-dark button-xlarge button-rounded mr-4">
                    <i class="icon-file-pdf1"></i> {{ __('general.download') }}
                </a>
            </div>
        </div>

    </div>

</section><!-- #content end -->
<!-- Footer -->
<footer id="footer">


    <!-- Copyrights -->
    @include('partials.footer')

</footer><!-- #footer end -->

@endsection