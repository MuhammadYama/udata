@extends('layout')

        @section('seo')
        <title>{{ __('general.title') }} | {{ __('general.work') }}</title>
        @endsection

@section('content')



<!-- Page Title -->
<section id="page-title">

    <div class="container clearfix text-center">
        <h1>{{ __('general.OurWorks') }}</h1>
        <span> </span>
        <!-- <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Shortcodes</a></li>
            <li class="breadcrumb-item active" aria-current="page">Sections</li>
        </ol> -->
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap" style="padding-bottom: 0;">

        <!-- Our Works
        ============================================= -->
        <div class="section nobg nobottommargin clearfix" style="padding-bottom: 0;">
            <div class="container center clearfix ">
                <div class="heading-block center noborder" data-heading="O">
                    <h3 class="nott ls0">{{ __('general.OurWorksList') }}</h3>
                </div>

                <!-- Portfolio Filter
                ============================================= -->
                <ul class="portfolio-filter style-2  clearfix" data-container="#portfolio">

                    <li class="activeFilter"><a href="#" data-filter="*">{{ __('general.showAll') }}</a></li>
                    @foreach($cats as $cat)
                    <li><a href="#" data-filter=".pf-{{$cat->category}}">{{$cat->translate()->category}}</a></li> 
                    @endforeach

                </ul><!-- #portfolio-filter end -->
            </div>

            <!-- Portfolio Items
            ============================================= -->
            <div id="portfolio" class="portfolio grid-container portfolio-nomargin clearfix">

                @foreach($works as $work)
                <article class="portfolio-item pf-media pf-{{$work->categoryz->category}}">
                    <div class="portfolio-image">
                        <a href="pages/{{$work->id}}/work">
                            <img src="{{ Voyager::image( $work->main_img ) }}" alt="Open Imagination">
                        </a>
                        <div class="portfolio-overlay">
                            <div class="portfolio-desc">
                                <h3><a href="pages/{{$work->id}}/work">{{$work->translate()->title}}</a></h3>
                                <span>{{$work->categoryz->translate()->category}}</span>
                            </div>
                        </div>
                    </div>
                </article>
                @endforeach
 

  

            </div><!-- #portfolio end -->
        </div>

    </div>

</section><!-- #content end -->


<!-- Footer -->
<footer id="footer">


    <!-- Copyrights -->
    @include('partials.footer')

</footer><!-- #footer end -->

@endsection