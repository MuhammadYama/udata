@extends('layout')

        @section('seo')
        <meta name="description" content="{{$seo->description ??''}}">
        <meta name="keywords" content="{{$seo->keywords ??''}}">
        <title>{{ __('general.title') }} | {{ __('general.about') }}</title>
        @endsection

@section('content')


<!-- Page Title  -->
<section id="page-title">

    <div class="container clearfix text-center">
        <h1>{{ __('general.about') }}</h1>
        <span> </span>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap" style="padding-bottom: 0;">

        <div class="container clearfix">

            <div class="col_two_fifth topmargin-sm  nobottommargin nbm-sm img-min" >
                <img src="{{ asset('esnaad/images/about.jpg')}}" alt="Image 1">
            </div>

            <div class="col_three_fifth nobottommargin col_last">

                <div class="heading-block topmargin-sm">
                    <h2>{{ __('general.about') }}</h2>
                </div>

                <p>{{$about->translate()->text}}</p>


            </div>

        </div>

        <div class="section parallax parallax-bg nomargin notopborder light py-sm" data-rellax-speed="2" style="background: url('{{asset('esnaad/images/bg-paralex.jpg')}}') center center; background-size:cover; padding: 100px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -200px;">
            <div class="container-fluid center clearfix">

                <div class="col_half nobottommargin ">

                    <div class="heading-block topmargin-sm">
                        <h2>{{ __('general.vision') }}</h2>
                    </div>

                    <p>{{$vision->translate()->text}}</p>

                </div>

                <div class="col_half nobottommargin col_last">

                    <div class="heading-block topmargin-sm">
                        <h2>{{ __('general.message') }}</h2>
                    </div>

                    <p>{{$mission->translate()->text}}</p>

                </div>

            </div>
        </div>

        <!-- partners -->
        <div class="section" style="margin-bottom: 0;margin-top: 0;">
            <div class="container">

                <div class="fancy-title title-center title-dotted-border">
                    <h3>{{ __('general.OurValues') }}</h3>
                </div>

                <div class="row">
                    @foreach($values as $value)
                    <div class="col-md-4">
                        <div class="feature-box fbox-border fbox-light fbox-effect">
                            <div class="fbox-icon">
                                <a href="#"><i class="{{$value->icon}}"></i></a>
                            </div>
                            <h3>&nbsp;</h3> 
                            <p>{{$value->translate()->text}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>

        

            </div>
        </div>

        <!-- Clients -->
        <div class="section nomargin noborder bgcolor dark" style="padding: 50px 0;">
            <div class="container  clearfix">

                <div class="heading-block  ">
                    <h2>{{ __('general.OurGoals') }}</h2>
                    <span> </span>
                </div>
                @foreach($goals as $goal)
                <div class="feature-box fbox-plain bottommargin">
                    <div class="fbox-icon" data-animate="fadeIn">
                        <a href="#"><i class="{{$goal->icon}} text-light" ></i></a>
                    </div>
                    <p class="text-light">{{$goal->translate()->text}}</p>
                </div>
                @endforeach
              
            </div>
        </div>
        <div class="section clearfix nopadding nomargin">
            <div class="container">
                <div class="promo  nomargin">
                    <h3><span class="text-light">{{ __('general.review') }}</span></h3>
                    <!-- <span>We strive to provide Our Customers with Top Notch Support to make their Theme Experience Wonderful</span> -->
                    <a href="#" class="button button-dark button-xlarge button-rounded pull-right">
                        <i class="icon-file-pdf1"></i> {{ __('general.download') }}
                    </a>
                </div>
            </div>
        </div>

    </div>

</section><!-- #content end -->


<!-- Footer -->
<footer id="footer">

    <!-- Subscribe -->
    @include('partials.subscribe')

    <!-- Copyrights -->
    @include('partials.footer')

</footer><!-- #footer end -->

@endsection