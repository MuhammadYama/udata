<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
@yield('seo')
@include('partials.head')

@yield('css')




</head>
<body class="stretched dark rtl" data-loader="7" data-loader-color="#751818" data-animation-in="fadeIn" data-speed-in="1500" data-animation-out="fadeOut" data-speed-out="800">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">
        
        <!-- Top Bar -->
        @include('partials.topbar')

        <!-- Header -->
        @include('partials.header')

        
        @yield('content')

	</div><!-- #wrapper end -->

	<!-- Go To Top -->
	<div id="gotoTop" class="icon-angle-up"></div>

    @include('partials.js')
    
    @yield('css')

    <div class="page-transition-wrap">
        <div class="css3-spinner">
            <div class="css3-spinner-grid-pulse">
                <div style="background-color:#751818;"></div>
                <div style="background-color:#751818;"></div>
                <div style="background-color:#751818;"></div>
                <div style="background-color:#751818;"></div>
                <div style="background-color:#751818;"></div>
                <div style="background-color:#751818;"></div>
                <div style="background-color:#751818;"></div>
                <div style="background-color:#751818;"></div>
                <div style="background-color:#751818;"></div>
            </div>
        </div>
    </div>
    <script src="//code.tidio.co/fqspewn431dwd0iqzqugpxapj4v6lj79.js"></script>
</body>
</html>