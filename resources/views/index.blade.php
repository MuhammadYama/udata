@extends('layout')

        @section('seo')
        <meta name="description" content="{{$seo->description ??''}}">
        <meta name="keywords" content="{{$seo->keywords ??''}}">
        <title>{{ __('general.title') }} | {{ __('general.home') }}</title>
        @endsection

@section('content')


<!-- Slider -->
@include('partials.slider')

<!-- Content -->
<section id="content">

    <div class="content-wrap pt-0" style="padding-bottom: 0;">

        <!-- youtube slides -->
        <section id="slider" class="slider-element boxed-slider video-slider"  style="background-image: url('{{ asset('esnaad/images/bg-bw.jpg')}} '); background-size:cover;" >

            <div class="container clearfix">

                <div id="oc-slider" class="owl-carousel carousel-widget" data-items="1" data-loop="true" data-nav="true" data-autoplay="5000" data-animate-in="fadeIn" data-animate-out="fadeOut" data-speed="800">
                @foreach($videos as $video)
                   {!! $video->link !!}
                @endforeach
                </div>


            </div>

        </section>

        <!-- Services -->
        <div class="section mt-0">
            <div class="container">

                <div class="fancy-title title-center title-dotted-border">
                    <h3>{{ __('general.OurService') }}</h3>
                </div>
                <div class="row">

                    <?php $n=0; ?>
                    @foreach($services as $service)
                    <?php $n++; ?>
                    <div class="col-md-4 mt-2">
                        <div class="feature-box fbox-center fbox-bg fbox-outline fbox-dark fbox-effect">
                            <div class="fbox-icon">
                                <a href="home-services.html"><i class="i-alt">{{$n}}</i></a>
                            </div>
                            <h3>{{$service->translate()->title}}<span class="subtitle">{!! str_limit($service->translate()->text , 100, '...') !!}</span></h3>
                        </div>
                    </div>
                    @endforeach 

                </div>

            </div>
        </div>

        <!-- features -->
        <div class="container clearfix">

                <div class="fancy-title title-center title-dotted-border">
                    <h3>{{ __('general.OurFeatures') }}</h3>
                </div>

                    <div class="row">
                        @foreach($features as $feature)
                        <div class="col-md-4 mb-5">
                            <div class="feature-box fbox-border fbox-light fbox-effect">
                                <div class="fbox-icon">
                                    <a href="#"><i class="icon-ok"></i></a>
                                </div>
                                <h3>{{$feature->translate()->title}}</h3>
                                <p>{!! str_limit($feature->translate()->text , 100, '...') !!}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
           
        </div>


        <!-- Clients -->
        <div class="section nomargin noborder bgcolor dark" style="padding: 50px 0;">
            <div class="container center clearfix">

                <div class="heading-block">
                    <h2>{{ __('general.OurClients') }}</h2>
                    <span> </span>
                </div>

                <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="true" data-items-xs="2" data-items-sm="3" data-items-lg="4" data-items-xl="5">
                    @foreach($clients as $client)
                    <div class="oc-item">
                        <a href="#"><img src="{{ Voyager::image( $client->img ) }}" alt="Image 1" width="200" height="200"></a>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>

        <!-- Testimonials -->
        <div class="container clearfix" style="padding: 50px 0;">

            <div class="fancy-title title-center title-dotted-border">
                <h3>{{ __('general.testimonials') }}</h3>
            </div>

            <div class="fslider testimonial testimonial-full bottommargin" data-animation="fade" data-arrows="false">
                <div class="flexslider">
                    <div class="slider-wrap">

                        @foreach($tests as $test)
                        <div class="slide">
                            <div class="testi-image">
                                <a href="#"><img src="{{ Voyager::image( $test->img ) }}" alt="Customer Testimonails"></a>
                            </div>
                            <div class="testi-content">
                                <p>{{$test->translate()->text}}</p>
                                <div class="testi-meta">
                                    {{$test->translate()->name}}
                                    <span>{{$test->translate()->title}}</span>
                                </div>
                            </div>
                        </div> 
                        @endforeach
                    </div>
                </div>
            </div>

        </div>

        <!-- contact -->
        <div class="section  nomargin notopborder dark" style="background-image: url('{{ asset('esnaad/images/bg.jpg')}} '); background-size:cover;" >
            <div class="container clearfix">

                <div class="emphasis-title center">
                    <h2 style="font-size: 52px;">{{ __('general.contactUs') }}</h2>
                </div>

                <div class="row">

                    <div class="col-lg-6 bottommargin">
                        <div class="testimonial">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d4418.388107078723!2d39.83530316845578!3d21.37388438979636!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x40a85646640cd21d!2si-ESNAAD!5e0!3m2!1sen!2ssa!4v1558522088727!5m2!1sen!2ssa" width="100%" height="133" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>

                    <div class="col-lg-6 bottommargin">
                        <div class="testimonial">

                            <form action ="{{ url('/contactUs') }}" method="POST">
                            {!! csrf_field() !!}
                                <div class="form-group">
                                    <label for="fullname">{{ __('general.name') }}</label>
                                    <input type="text" class="form-control" id="fullname"  name="name" placeholder="{{ __('general.name') }}">
                                    <small id="emailHelp" class="form-text text-muted"> </small>
                                </div>
                                <div class="form-group">
                                    <label for="phone">{{ __('general.phone') }}</label>
                                    <input type="number" class="form-control" id="phone" name="phone" placeholder="{{ __('general.phone') }}">
                                </div>
                                <div class="form-group">
                                    <label for="email">{{ __('general.email') }}</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="{{ __('general.email') }}">
                                </div>
                                <div class="form-group">
                                    <label for="subject">{{ __('general.subject') }}</label>
                                    <input type="text" class="form-control" id="subject" name="subject" placeholder="{{ __('general.subject') }}">
                                </div>
                                <div class="form-group">
                                    <label for="branch">{{ __('general.branch') }}</label>
                                    <select  class="form-control" id="branch" name="branch">
                                        <option >{{ __('general.select') }}</option>
                                        <option value="فرع جدة">{{ __('general.jeddah') }}</option>
                                        <option value="فرع مكة">{{ __('general.makkah') }}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="msg">{{ __('general.message') }}</label>
                                    <textarea  class="form-control" id="" rows="5" name="message" placeholder="{{ __('general.message') }}"></textarea>
                                </div>
                                <button type="submit" class="button button-rounded  mt-3">{{ __('general.send') }}</button>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>

</section><!-- #content end -->

<!-- Footer -->
<footer id="footer">

    <!-- Subscribe -->
    @include('partials.subscribe')

    <!-- Copyrights -->
    @include('partials.footer')

</footer><!-- #footer end -->
@endsection