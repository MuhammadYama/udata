
<div class="container" style="border-bottom: 1px solid rgba(0,0,0,0.06);">

    <div class="footer-widgets-wrap clearfix">

        <div class="col_full">

            <div class="widget clearfix">

                <div class="widget-subscribe-form-result"></div>
                <form id="widget-subscribe-form" action ="{{ url('/subscribe') }}" method="POST" class="nobottommargin row clearfix">
                                    {!! csrf_field() !!}
                <div class="col-lg-9">
                        <input type="email" id="widget-subscribe-form-email" name="email" class="sm-form-control required email" placeholder="{{ __('general.email') }}">
                    </div>
                    <div class="col-lg-3">
                        <button class="button button-rounded nomargin center btn-block" type="submit">{{ __('general.subscribe') }}</button>
                    </div>
                </form>

            </div>

        </div>

    </div><!-- .footer-widgets-wrap end -->

</div>