<section id="slider" class="slider-element swiper_wrapper full-screen clearfix" data-loop="true" data-autoplay="5000">

    <div class="swiper-container swiper-parent">
        <div class="swiper-wrapper">

        @foreach($sliders as $slider)
            <div class="swiper-slide" style="background-image: url('{{ Voyager::image( $slider->img ) }} ');">
                <div class="container clearfix">
                    <div class="slider-caption slider-caption-right" style="max-width: 700px;">
                        <h2 data-animate="flipInX">{{$slider->main_title}}<span>.</span></h2>
                        <p class="d-none d-sm-block" data-animate="flipInX" data-delay="500">{{$slider->sub_title}}</p>
                    </div>
                </div>
            </div>
        @endforeach

        </div>

    </div>

</section><!-- #slider end -->