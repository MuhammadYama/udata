<div id="top-bar">

    <div class="container clearfix">

        <div class="col_half d-block nobottommargin">

            <!-- Top Links
            ============================================= -->
            <div class="top-links">
                <ul>
                    <li><a href="tel:{{$phone->account ??''}}"><i class="icon-phone3"></i>{{$phone->account ??''}}</a></li>
                    <li><a href="mailto:{{$email->account ??''}}" class="nott"><i class="icon-envelope2"></i> {{$email->account ??''}}</a></li>
                    @if(App::getLocale()=='ar')
                    <li><a href="{{url('locale/en')}}"> 
                        Eng</a> 
                    </li>
                    @else
                    <li><a href="{{url('locale/ar')}}"> 
                        عربي</a> 
                    </li>
                    @endif
                </ul>
            </div><!-- .top-links end -->

        </div>

        <div class="col_half col_last  nobottommargin">

            
            <!-- Top Social
            ============================================= -->
            <div id="top-social">
                <ul>
                    <li><a href="{{$facebook->account ??''}}" class="si-facebook" target="_blank"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
                    <li><a href="{{$twitter->account ??''}}" class="si-twitter" target="_blank"><span class="ts-icon"><i class="icon-twitter"></i></span><span class="ts-text">Twitter</span></a></li>
                    <li><a href="{{$youtube->account ??''}}" class="si-youtube" target="_blank"><span class="ts-icon"><i class="icon-youtube"></i></span><span class="ts-text">Youtube</span></a></li>
                    <li><a href="{{$insta->account ??''}}" class="si-instagram" target="_blank"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
                    <li><a href="tel:{{$phone->account ??''}}" class="si-call"><span class="ts-icon"><i class="icon-call"></i></span><span class="ts-text">{{$phone->account ??''}}</span></a></li>
                    <li><a href="mailto:{{$email->account ??''}}" class="si-email3"><span class="ts-icon"><i class="icon-envelope-alt"></i></span><span class="ts-text">{{$email->account ??''}}</span></a></li>
                </ul>
            </div><!-- #top-social end -->

        </div>

    </div>

</div><!-- #top-bar end -->