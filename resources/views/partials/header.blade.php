@php
use App\Service;
@endphp
<header id="header">

    <div id="header-wrap">

        <div class="container clearfix">

            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

            <!-- Logo
            ============================================= -->
            <div id="logo">
                <a href="{{url('/')}}" class=""><img src="{{asset('esnaad/images/udata/logo-small.png')}}" alt="Canvas Logo"></a>
                <!-- <a href="index.html" class="retina-logo"><img src="demos/medical/images/logo-medical@2x.png" alt="Canvas Logo"></a> -->
                <!-- <a href="index.html" class="standard-logo pt-4">UDATA</a> -->
                
            </div><!-- #logo end -->
            @php
                $services = Service::get();
            @endphp
            <!-- Primary Navigation
            ============================================= -->
            <nav id="primary-menu" class="style-3">

                <ul>
                    <li class="{{ Request::is('/') ? 'current   ' : '' }}"><a href="{{url('/')}}"><div>{{ __('general.home') }}</div></a></li>
                    <li class="{{ Request::is('about') ? 'current' : '' }}"><a href="{{url('about')}}"><div>{{ __('general.about') }}</div></a></li>
                    <!-- <li class=""><a href="home-services.html"><div>Our Services </div></a></li> -->
                    <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                            {{ __('general.service') }}
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                @foreach($services as $service)
                                <a class="dropdown-item {{ Request::is('pages/'.$service->id.'/service') ? 'current' : '' }}" href="{{url('pages/'.$service->id.'/service')}}">{{$service->translate()->title}}</a> 
                                @endforeach
                            </ul>
                        </li>
                    <li class="{{ Request::is('works') ? 'current' : '' }}"><a href="{{url('works')}}"><div>{{ __('general.work') }} </div></a></li>
                    <li class="{{ Request::is('contact') ? 'current' : '' }}"><a href="{{url('contact')}}"><div>   {{ __('general.contact') }} </div></a></li>
                </ul>

            </nav><!-- #primary-menu end -->

        </div>

    </div>

</header><!-- #header end -->