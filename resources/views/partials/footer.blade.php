
<div id="copyrights" class="nobg" style="background-color: #111 !important;">

    <div class="container clearfix">

        <div class="col_half cr">
            Copyrights &copy; 2019 All Rights Reserved for Udata.<br>
          
        </div>

        <div class="col_half col_last esnaad-link">
            <div class="copyrights-menu copyright-links clearfix">
                <div class="copyright-links">Developed with LOVE by <a href="https://www.i-Esnaad.com"> i-Esnaad</a></div>

                <a href="{{$facebook->account ??''}}"><i class="icon-facebook"></i></a>.<a href="{{$twitter->account ??''}}"><i class="icon-twitter"></i></a>.<a href="{{$youtube->account ??''}}"><i class="icon-youtube"></i></a>.<a href="{{$insta->account ??''}}"><i class="icon-instagram2"></i></a>
                
            </div>
        </div>

    </div>

</div><!-- #copyrights end -->