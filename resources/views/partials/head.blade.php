
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="SemiColonWeb" />

<!-- Stylesheets
============================================= -->
<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Montserrat:400,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ asset('esnaad/css/bootstrap.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('esnaad/style.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('esnaad/css/swiper.css') }}" type="text/css" />

<!-- Medical Demo Specific Stylesheet -->
<!-- <link rel="stylesheet" href="demos/medical/medical.css" type="text/css" /> -->
<!-- / -->

<link rel="stylesheet" href="{{ asset('esnaad/css/dark.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('esnaad/css/font-icons.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('esnaad/demos/medical/css/medical-icons.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('esnaad/css/animate.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('esnaad/css/magnific-popup.css') }}" type="text/css" />

<link rel="stylesheet" href="{{ asset('esnaad/demos/medical/fonts.css') }}" type="text/css" />

<link rel="stylesheet" href="{{ asset('esnaad/css/responsive.css') }}" type="text/css" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<link rel="stylesheet" href="{{ asset('esnaad/css/colors.php?color=DE6262') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('esnaad/css/colors.css') }}" type="text/css" />
@if ( App::getLocale()=='ar')

        @include('partials.head-rtl')
@endif
<!-- Document Title
============================================= -->

<link rel="stylesheet" href="{{ asset('esnaad/css/custom.css') }}" type="text/css" />
@if ( App::getLocale()=='ar')
<link rel="stylesheet" href="{{ asset('esnaad/css/custom-rtl.css') }}" type="text/css" />
@endif