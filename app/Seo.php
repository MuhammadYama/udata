<?php

namespace App; 
use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    
    protected $fillable = ['page_title','desc','keywords'];
}
