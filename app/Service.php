<?php

namespace App;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use Translatable;
    protected $translatable = ['title','text'];

    protected $fillable = ['title','text','img'];

    public function featurez()
    {
        return $this->hasMany('App\ServiceFeature');
    }

    public function partnerz()
    {
        return $this->hasMany('App\ServicePartner');
    }
}
