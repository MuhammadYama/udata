<?php

namespace App;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class OurClient extends Model
{
    use Translatable;
    protected $translatable = ['title'];

    protected $fillable = ['title','img'];
}
