<?php

namespace App;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;


class Slider extends Model
{
    use Translatable;
    protected $translatable = ['main_title','sub_title'];

    protected $fillable = ['main_title','sub_title','img','link'];
}
