<?php

namespace App;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class OurWork extends Model
{
    use Translatable;
    protected $translatable = ['title','text'];

    protected $fillable = ['title','category','text','main_img','completion'];

    public function categoryz()
    {
        return $this->belongsTo('App\Category', 'category');
    }

    public function imagez()
    {
        return $this->hasMany('App\OurWorkImage');
    }

    public function listz()
    {
        return $this->hasMany('App\OurWorkList');
    }
}
