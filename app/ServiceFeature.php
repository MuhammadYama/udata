<?php

namespace App;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use App;

class ServiceFeature extends Model
{
    use Translatable;
    protected $translatable = ['text'];

    protected $fillable = ['text','eng_text','service_id'];


    public function servicez()
    {
        return $this->belongsTo('App\Service', 'service_id');
    }

    public function getLocalFeature()
     {
      if(App::getLocale()=='en')
         return $this->eng_text;
         return $this->text;
     }
     
}
