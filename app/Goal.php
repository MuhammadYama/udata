<?php

namespace App;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Goal extends Model
{
    use Translatable;
    protected $translatable = ['text'];

    protected $fillable = ['text','icon'];
}
