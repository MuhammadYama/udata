<?php

namespace App;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class WorkingHour extends Model
{
    use Translatable;
    
    protected $translatable = ['day'];

    protected $fillable = ['day','open','close','branch'];
}
