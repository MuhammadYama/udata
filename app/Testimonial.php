<?php

namespace App;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    use Translatable;
    protected $translatable = ['name','title','text'];

    protected $fillable = ['name','title','text','img'];
}
