<?php

namespace App;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class OurFeature extends Model
{
    use Translatable;
    protected $translatable = ['title','text'];

    protected $fillable = ['title','text'];
}
