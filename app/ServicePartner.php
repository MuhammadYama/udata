<?php

namespace App;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class ServicePartner extends Model
{
    use Translatable;
    protected $translatable = ['title'];
    
    protected $fillable = ['title','img','service_id'];

    public function servicez()
    {
        return $this->belongsTo('App\Service', 'service_id');
    }
}
