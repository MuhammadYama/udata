<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OurWork;
use App\OurWorkImage;
use App\OurWorkList;

use App\Service;
use App\ServiceFeature;
use App\ServicePartner;

class WorkController extends Controller
{
    public function images($id)
    {   
        $work = OurWork::where('id', $id)->first();
        $images = OurWorkImage::where('work_id', $id)->get();
        return view('vendor.voyager.our-works.images',compact('work','images'));
    }

    public function lists($id)
    {   
        $work = OurWork::where('id', $id)->first();
        $lists = OurWorkList::where('work_id', $id)->get();
        return view('vendor.voyager.our-works.lists',compact('work','lists'));
    }

    public function add_image($id)
    {

        $work = OurWork::where('id', $id)->first(); 

        return view('vendor.voyager.our-works.add_image', compact('work'));

    }

    public function addImage(Request $request)
    {  

        // dd('gg');
        $work_id=request('work_id');
        $file_name = request('img');

        $path='our-work-images/files';
        $path= $request->img->store($path, 'public');
        
        $image =new OurWorkImage;
        $image->work_id= request('work_id'); 
        $image->img= $path;    
        $image->save();

        return redirect("admin/our-works/images/$work_id");
     
    }

    public function deleteImage($id, $image_id)
    { 
        // dd('kj');
        $image = OurWorkImage::where('id', $image_id)->delete(); 
        return back();
    }


    public function add_list($id)
    {

        $work = OurWork::where('id', $id)->first(); 

        return view('vendor.voyager.our-works.add_list', compact('work'));

    }

    
    public function addList(Request $request)
    {  
 
        $work_id=request('work_id');

        $list =new OurWorkList;
        $list->work_id= request('work_id'); 
        $list->text= request('text');    
        $list->eng_text= request('eng_text'); 
        $list->save();

        return redirect("admin/our-works/$work_id/lists");
     
    }

    public function deleteList($id, $list_id)
    { 
        // dd('kj');
        $list = OurWorkList::where('id', $list_id)->delete(); 
        return back();
    }


    public function features($id)
    {   
        $service = Service::where('id', $id)->first();
        $features = ServiceFeature::where('service_id', $id)->get();
        return view('vendor.voyager.services.features',compact('service','features'));
    }

    public function add_feature($id)
    {

        $service = Service::where('id', $id)->first(); 

        return view('vendor.voyager.services.add_feature', compact('service'));

    }


    public function addFeature(Request $request)
    {  
 
        $service_id=request('service_id');

        $feature =new ServiceFeature;
        $feature->service_id= request('service_id'); 
        $feature->text= request('text');    
        $feature->eng_text= request('eng_text');  
        // dd($feature->eng_text);
        $feature->save();

        return redirect("admin/services/$service_id/features");
     
    }

    public function deleteFeature($id, $feature_id)
    { 
        // dd('kj');
        $feature = ServiceFeature::where('id', $feature_id)->delete(); 
        return back();
    }


    public function partners($id)
    {   
        $service = Service::where('id', $id)->first();
        $partners = ServicePartner::where('service_id', $id)->get();
        return view('vendor.voyager.services.partners',compact('service','partners'));
    }

    public function add_partner($id)
    {

        $service = Service::where('id', $id)->first();

        return view('vendor.voyager.services.add_partner', compact('service'));

    }


    public function addPartner(Request $request)
    {  

        // dd('gg');
        $service_id=request('service_id');
        $file_name = request('img');

        $path='service-partners/files';
        $path= $request->img->store($path, 'public');
        
        $partner =new ServicePartner;
        $partner->service_id= request('service_id'); 
        $partner->title= request('title'); 
        $partner->img= $path;    
        $partner->save();

        return redirect("admin/services/$service_id/partners");
     
    }

    public function deletePartner($id, $partner_id)
    { 
        // dd('kj');
        $partner = ServicePartner::where('id', $partner_id)->delete(); 
        return back();
    }

}
