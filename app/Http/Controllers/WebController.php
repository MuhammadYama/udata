<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\OurFeature;
use App\OurClient;
use App\Testimonial;
use App\Text;
use App\Value;
use App\Goal;
use App\ServiceFeature;
use App\ServicePartner;
use App\Category;
use App\OurWork;
use App\OurWorkImage;
use App\OurWorkList;
use App\Youtube;
use App\ContactUs;
use App\Newsletter;
use App\Slider;
use App\Contact;
use App\WorkingHour;
use App\Seo;

class WebController extends Controller
{
    public function index()
    {   
        $services = Service::get();
        $features = OurFeature::get();
        $clients = OurClient::get();
        $tests = Testimonial::get();
        $videos = Youtube::get();
        $sliders = Slider::get();
        $facebook = Contact::where('type','facebook')->first();
        $twitter = Contact::where('type','twitter')->first();
        $insta = Contact::where('type','instagram')->first();
        $youtube = Contact::where('type','youtube')->first();
        $phone = Contact::where('type','phone')->first();
        $email = Contact::where('type','email')->first();
        $seo = Seo::where('page_title','home')->first();
        return view('index',compact('services','features','clients','tests','videos','sliders','facebook','twitter',
                    'insta','youtube','phone','email','seo'));
    }


    public function about()
    {   
        $about = Text::where('type','about')->first(); 
        $vision = Text::where('type','vision')->first(); 
        $mission = Text::where('type', 'msg')->first();
        $values = Value::get();
        $goals = Goal::get();
        $facebook = Contact::where('type','facebook')->first();
        $twitter = Contact::where('type','twitter')->first();
        $insta = Contact::where('type','instagram')->first();
        $youtube = Contact::where('type','youtube')->first();
        $phone = Contact::where('type','phone')->first();
        $email = Contact::where('type','email')->first();
        $seo = Seo::where('page_title','about')->first();
        return view('pages/about',compact('about','vision','mission','values','goals','facebook','twitter',
        'insta','youtube','phone','email','about'));
    }

    public function service(Service $service)
    {   
        $features = ServiceFeature::where('service_id', $service->id)->get();
        $partners = ServicePartner::where('service_id', $service->id)->get();
        $facebook = Contact::where('type','facebook')->first();
        $twitter = Contact::where('type','twitter')->first();
        $insta = Contact::where('type','instagram')->first();
        $youtube = Contact::where('type','youtube')->first();
        $phone = Contact::where('type','phone')->first();
        $email = Contact::where('type','email')->first();
        $seo = Seo::where('page_title','service')->first();
        return view('pages/service',compact('service','features','partners','facebook','twitter',
        'insta','youtube','phone','email','seo'));
    }

    public function works()
    {   
        $cats = Category::get(); 
        $works = OurWork::get(); 
        $facebook = Contact::where('type','facebook')->first();
        $twitter = Contact::where('type','twitter')->first();
        $insta = Contact::where('type','instagram')->first();
        $youtube = Contact::where('type','youtube')->first();
        $phone = Contact::where('type','phone')->first();
        $email = Contact::where('type','email')->first();
        $seo = Seo::where('page_title','work')->first();
        return view('pages/works',compact('cats','works','facebook','twitter',
        'insta','youtube','phone','email','seo'));
    }

    public function work(OurWork $work)
    {   
        $images = OurWorkImage::where('work_id', $work->id)->get();
        $lists = OurWorkList::where('work_id', $work->id)->get();
        $facebook = Contact::where('type','facebook')->first();
        $twitter = Contact::where('type','twitter')->first();
        $insta = Contact::where('type','instagram')->first();
        $youtube = Contact::where('type','youtube')->first();
        $phone = Contact::where('type','phone')->first();
        $email = Contact::where('type','email')->first();
        $seo = Seo::where('page_title','work')->first();
        return view('pages/work',compact('work','images','lists','facebook','twitter',
        'insta','youtube','phone','email','seo'));
    }

    public function contact()
    {   
        $facebook = Contact::where('type','facebook')->first();
        $twitter = Contact::where('type','twitter')->first();
        $insta = Contact::where('type','instagram')->first();
        $youtube = Contact::where('type','youtube')->first();
        $phone = Contact::where('type','phone')->first();
        $email = Contact::where('type','email')->first();
        $workJ = WorkingHour::where('branch', 0)->get();
        $workM = WorkingHour::where('branch', 1)->get();
        $seo = Seo::where('page_title','contact')->first();
        return view('pages/contact',compact('facebook','twitter',
        'insta','youtube','phone','email','workJ','workM','seo'));
    }

    public function store(Request $request)
    {

        
        $contact =new ContactUs;
        $contact->name= request('name');
        $contact->phone= request('phone');
        $contact->email= request('email');
        $contact->branch= request('branch');
        $contact->subject= request('subject');
        $contact->message= request('message');
        $contact->save();
 
        return redirect('/')->with('message','تم الإرسال بنجاح');
    }

    public function subscribe(Request $request)
    {

        
        $new =new Newsletter; 
        $new->email= request('email'); 
        $new->save();
 
        return back();
    }
}
