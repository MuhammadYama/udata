<?php

namespace App;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    use Translatable;
    protected $translatable = ['title','type','text'];

    protected $fillable = ['title','type','text'];
}
