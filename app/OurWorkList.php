<?php

namespace App;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use App;

class OurWorkList extends Model
{
    use Translatable;
    protected $translatable = ['text'];

    protected $fillable = ['text','eng_text','work_id'];


    public function ourWork()
    {
        return $this->belongsTo('App\OurWork', 'work_id');
    }


    public function getLocalList()
     {
      if(App::getLocale()=='en')
         return $this->eng_text;
         return $this->text;
     }
}
