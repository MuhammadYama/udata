<?php

namespace App; 
use Illuminate\Database\Eloquent\Model;

class OurWorkImage extends Model
{

    protected $fillable = ['img','work_id'];

    public function ourWork()
    {
        return $this->belongsTo('App\OurWork', 'work_id');
    }
}
